from django.db import models
from django.contrib.auth.models import AbstractUser, User
import django_filters

# Create your models here.


class CustomUser(AbstractUser):
    pass


class Person(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

    # @property
    # def full_name(self):
    #     return '%s %s' %(self.first_name,self.last_name)


class ProductFilter(django_filters.FilterSet):
    class Meta:
        model = CustomUser
        fields = ['first_name', 'last_name']


class F(django_filters.FilterSet):
    username = django_filters.CharFilter(method='my_custom_filter')

    class Meta:
        model = CustomUser
        fields = ['username']

    def my_custom_filter(self, queryset, name, value):
        return queryset.filter(**{
            name: value,
        })