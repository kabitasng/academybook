from django.contrib.auth.models import User
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView,ListView,DetailView
from .forms import CustomUserCreationForm
# from .filters import UserFilter
from .models import CustomUser, ProductFilter
# from .serializers import PurchaseSerializer
from rest_framework import generics

from django_filters.rest_framework import DjangoFilterBackend
#
# from django.contrib.auth.models import User
# from django.shortcuts import render
# from django_filters import rest_framework as filters
# from rest_framework.filters import SearchFilter, OrderingFilter


class SignUp(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

#
# class UserListView(generics.ListAPIView):
#     serializer_class = UserSerializer
#     queryset = User.objects.all()
#     filter_backends = (DjangoFilterBackend,)
#     filter_field

#
# def search(request):
#     user_list = CustomUser.objects.all()
#     user_filter = UserFilter(request.GET, queryset=user_list)
#     return render(request, 'search/user_list.html', {'filter': user_filter})


def dashboard(request):
    return render(request, 'dashboard.html')


def show(request):
    users = User.objects.all()
    current_user = User.objects.get(username=request.user.username)
    try:
        if current_user.groups.get().id == CustomUser.objects.get(name="Admin").id:
            context = {
                'users': users
            }
            return render(request, "dashboard.html", context)
        else:
            return render(request, "normal_dashboard.html", {'users': users})

    except Exception as e:
        return render(request, "normal_dashboard.html", {'users': users})


def product_list(request):
    f = ProductFilter(request.GET, queryset=User.objects.all())
    return render(request, 'dcu/templates/filter.html', {'filter': f})