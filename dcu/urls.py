"""dcu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include,reverse
from django.views.generic.base import TemplateView
from django_filters.views import FilterView

# from dcu.users.models import CustomUser

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', TemplateView.as_view(template_name='homepage.html'), name='homepage'),
    path('accounts/', include('users.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    # path('calendar/', include('calendarium.urls')),
    # User management
    path('users/', include('users.urls')),
    path('accounts/', include('allauth.urls')),

    # path('list/', FilterView.as_view(model=CustomUser)),
]
